 /*
* SD Card Module Interfacing with ESP32 Wifi + Bluetooth Module
 * https://www.electroniclinic.com/sd-card-module-with-arduino-esp32-arduino-data-logger-esp32-data-logger/#SD_Card_Module_with_Arduino_ESP32
 * https://www.luisllamas.es/buffer-circular-arduino/
 */
 
#include "FS.h"
#include "SD.h"
#include <SPI.h>
#include "SoftwareSerial.h"
#include <HardwareSerial.h>

#define SD_CS 5
String dataMessage;
 
#define RXD2 16
#define TXD2 17
// SoftwareSerial ATSerial(5, 12);
HardwareSerial MySerial(2);


String Vrdata;
char header[2];
uint8_t header_available = 0;
uint8_t s1_flag = 0;

String Vrdata2;
uint8_t header_available2 = 0;
uint8_t s2_flag = 0;

#define UART_DEBUG 

const int circularBufferLength = 2;
char circularBuffer[circularBufferLength];
int circularBufferIndex = 0;

char circularBuffer2[circularBufferLength];
int circularBufferIndex2 = 0;

void appendToBuffer(char value, char* circularBuffer, int* circularBufferIndex)
{
   circularBuffer[*circularBufferIndex] = value;
   *circularBufferIndex = *circularBufferIndex + 1;
   if (*circularBufferIndex >= circularBufferLength) *circularBufferIndex = 0;
}
void getFromBuffer(char* out, int outLength, char* circularBuffer, int* circularBufferIndex)
{
   int readIndex = (*circularBufferIndex - outLength + circularBufferLength) % circularBufferLength;
   for (int iCount = 0; iCount < outLength; iCount++)
   {
      if (readIndex >= circularBufferLength) readIndex = 0;
      out[iCount] = circularBuffer[readIndex];
      readIndex++;
   }
}
void getFromBufferInvert(char* out, int outLength, char* circularBuffer)
{
   int readIndex = circularBufferIndex - 1;
   for (int iCount = 0; iCount < outLength; iCount++)
   {
      if (readIndex < 0) readIndex = circularBufferLength - 1;
      out[iCount] = circularBuffer[readIndex];
      readIndex--;
   }
}
void printArray(char* x, int length)
{
   for (int iCount = 0; iCount < length; iCount++)
   {
      Serial.print(x[iCount]);
      Serial.print(',');
   }
   Serial.println();
}


// Write to the SD card (DON'T MODIFY THIS FUNCTION)
void writeFile(fs::FS &fs, const char * path, const char * message) {
  #ifdef UART_DEBUG
    Serial.printf("Writing file: %s\n", path);
  #endif
  File file = fs.open(path, FILE_WRITE);
  if(!file) {
    #ifdef UART_DEBUG
      Serial.println("Failed to open file for writing");
    #endif
    return;
  }
  if(file.print(message)) {
    #ifdef UART_DEBUG
      Serial.println("File written");
    #endif
  } else {
    #ifdef UART_DEBUG
      Serial.println("Write failed");
    #endif
  }
  file.close();
}
// Append data to the SD card (DON'T MODIFY THIS FUNCTION)
void appendFile(fs::FS &fs, const char * path, const char * message) {
  #ifdef UART_DEBUG
    Serial.printf("Appending to file: %s\n", path);
  #endif
  File file = fs.open(path, FILE_APPEND);
  if(!file) {
  #ifdef UART_DEBUG
    Serial.println("Failed to open file for appending");
  #endif
  return;
  }
  if(file.print(message)) {
  #ifdef UART_DEBUG
    Serial.println("Message appended");
  #endif
  } else {
  #ifdef UART_DEBUG
    Serial.println("Append failed");
  #endif
  }
  file.close();
}

// Write the sensor readings on the SD card
void logSDCard() {
  dataMessage =  String(Vrdata);// + "\n";
  // dataMessage = Vrdata + "\n";
  #ifdef UART_DEBUG
    Serial.print("Save data: ");
    Serial.println(dataMessage);
  #endif
  appendFile(SD, "/data.txt", dataMessage.c_str());
}



void setup() {
  // Serial.begin(230400);
  // pinMode(13, OUTPUT); // i use this to supply 3.3volts to the variable resistor
  // digitalWrite(13, HIGH);
  
  // ATSerial.begin(9600);//, SERIAL_8N1, RXD2, TXD2
  Serial.begin(9600);//, SERIAL_8N1, RXD2, TXD2
  MySerial.begin(9600, SERIAL_8N1, 5, 12);

  /*Initialize SD card*/
  SD.begin(SD_CS);  
  if(!SD.begin(SD_CS)) {
    #ifdef UART_DEBUG
      Serial.println("Card Mount Failed");
    #endif
    return;
  }
  uint8_t cardType = SD.cardType();
  if(cardType == CARD_NONE) {
    #ifdef UART_DEBUG
      Serial.println("No SD card attached");
    #endif
    return;
  }
  #ifdef UART_DEBUG
    Serial.println("Initializing SD card...");
  #endif
  if (!SD.begin(SD_CS)) {
    #ifdef UART_DEBUG
      Serial.println("ERROR - SD card initialization failed!");
    #endif
    return;    // init failed
  }
  File file = SD.open("/data.txt");
  if(!file) {
    #ifdef UART_DEBUG
      Serial.println("File doens't exist");
      Serial.println("Creating file...");
    #endif
    writeFile(SD, "/data.txt", "ESP32 and SD Card \r\n");
  }
  else {
    #ifdef UART_DEBUG
      Serial.println("File already exists");  
    #endif
  }
  file.close();
}
void loop() {

  if (MySerial.available()) {
    char c = (char(MySerial.read()));
    #ifdef UART_DEBUG
      MySerial.println(c);  
    #endif
    if (!header_available){
      #ifdef UART_DEBUG
        Serial.println("header NOT available");  
      #endif
      appendToBuffer(c, circularBuffer, &circularBufferIndex);
      #ifdef UART_DEBUG
        Serial.println("Buffer circular:");  
        Serial.println(circularBuffer); 
      #endif
      if ((circularBuffer[0] == 'M') & (circularBuffer[1] == ':')){
        header_available = 1;
        Vrdata =""; //Clear string
      }
    }
    else{
      #ifdef UART_DEBUG
        Serial.println("header available");  
      #endif
      // Vrdata = Serial.readStringUntil('\r');
      Vrdata += c;
      appendToBuffer(c, circularBuffer, &circularBufferIndex);
      if ( c == '\n'){//(circularBuffer[1] == '\n')){ //(circularBuffer[0] == '\r') &
        logSDCard();
        header_available = 0;
      #ifdef UART_DEBUG
        Serial.println("header NOT available AGAIN");  
      #endif
      }
    }
  }
}


//   if (Serial2.available()) {
//     char c2 = (char(Serial2.read()));
//     #ifdef UART_DEBUG
//       Serial2.println(c2);  
//     #endif
//     if (!header_available2){
//       #ifdef UART_DEBUG
//         Serial2.println("header NOT available");  
//       #endif
//       appendToBuffer(c2, circularBuffer2, &circularBufferIndex2);
//       #ifdef UART_DEBUG
//         Serial2.println("Buffer circular:");  
//         Serial2.println(circularBuffer2); 
//       #endif
//       if ((circularBuffer2[0] == 'M') & (circularBuffer2[1] == ':')){
//         header_available2 = 1;
//         Vrdata2 =""; //Clear string
//       }
//     }
//     else{
//       #ifdef UART_DEBUG
//         Serial2.println("header available");  
//       #endif
//       // Vrdata = Serial.readStringUntil('\r');
//       Vrdata += c2;
//       appendToBuffer(c2, circularBuffer2, &circularBufferIndex2);
//       if ( c2 == '\n'){//(circularBuffer[1] == '\n')){ //(circularBuffer[0] == '\r') &
//         logSDCard();

//         header_available2 = 0;
//       #ifdef UART_DEBUG
//         Serial2.println("header NOT available AGAIN");  
//       #endif
//       }
//     }
//   }
// }